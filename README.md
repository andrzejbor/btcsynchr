# btcsynchr

Synchronization over USB flash drive.

Let imagine we have on disk over 200 GB of blockchain, which are downloaded long time. We want its copy on second computer. 
First it maybe Linux on VirtualBox on PC, second: Linux in laptop.<br/>
We copy to big USB flash disk. Now each week..each month we want synchronize. Needs copy new file and delete old one. 
Btcsynchr is helpful for it.<br/>
We create alphabetical list of files from laptop<br/>
./btcsynchr -list /home/andrzej/.bitcoin > laptop.txt<br/>
and PC:<br/>
./btcsynchr -list /home/host/.bitcoin/ > pcnow.txt<br/>
we create diff file:<br/>
./btcsynchr -compare pcnow.txt laptop.txt > diffs.txt<br/>
On source computer we copy to USB flash disk,
on destination we delete:<br/>
./btcsynchr -delete diffs.txt /home/andrzej/.bitcoin<br/>
and we copy to destination from USB disk.<br/>
We need set time, because copy from different filesystem makes difference, for example in Poland, summertime give 2 hours  = 7200 to 7202 seconds of difference.
First we use initsettime.<br/>
./btcsynchr -initsettime pcnow.txt /home/andrzej/.bitcoin<br/>
for forgetful, if not uses initsettime, next time need<br/> 
./btcsynchr -forgetsettime pcnow.txt /home/andrzej/.bitcoin 7200 7202<br/>
each difference need<br/>
./btcsynchr -settime diffs.txt /home/andrzej/.bitcoin<br/>

<br/>
Compilation:<br/>
Install Boost:<br/>
sudo apt-get install libboost-all-dev<br/>

cmake .<br/>
make<br/>
