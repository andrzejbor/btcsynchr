#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <time.h>
#include <utime.h>
#include <sys/stat.h>
#include "boost/filesystem.hpp"
#include <boost/lexical_cast.hpp>

struct FileInfo {
	std::string fname;
	int64_t size;
	std::time_t time;
};

bool compareByName(const FileInfo &a, const FileInfo &b)
{
	return a.fname < b.fname;
}

std::vector<FileInfo> get_dir(const std::string dir, const std::string insidepath)
{
	std::vector<FileInfo> filelist;
	std::vector<std::string> dirlist;
	if (!dir.empty())
	{
		namespace fs = boost::filesystem;

		fs::path p(dir + insidepath);
		fs::directory_iterator end;

		for (fs::directory_iterator it(p); it != end; ++it)
		{
			const fs::path p = (*it);
			std::time_t t = boost::filesystem::last_write_time(p);
			if (fs::is_regular_file(p))
			{
				FileInfo fi;
				fi.fname = p.filename().string();
				fi.size = fs::file_size(p);
				fi.time = t;
				filelist.push_back(fi);
			}
			else dirlist.push_back(p.filename().string());
		}
	}
	sort(filelist.begin(), filelist.end(), compareByName);
    std::vector<FileInfo> vec;
    for (int i = 0; i<filelist.size(); i++)
	{
		FileInfo fi = filelist[i];
        FileInfo rec;
        rec.fname = insidepath + "/" + fi.fname;
        rec.size = fi.size;
        rec.time = fi.time;
        vec.push_back(rec);
	}
	sort(dirlist.begin(), dirlist.end());
	for (int i=0; i<dirlist.size(); i++)
	{
        std::vector<FileInfo> subvec = get_dir(dir, insidepath + "/" + dirlist[i]);
        vec.insert(std::end(vec), std::begin(subvec), std::end(subvec));
	}
	return vec;
}

void info()
{
    printf("-list directory: can be redirected to file\n");
    printf("-compare srclist destlist: can be redirected to file\n");
    printf("-delete difflist destdir\n");
    printf("-copy difflist srcdir destdir\n");
    printf("-settime difflist destdir\n");
    printf("-initsettime srclist destdir\n");
    printf("-forgetsettime srclist destdir fromTimeDiff toTimeDiff\n");
}

void list(const std::string dir)
{
    std::vector<FileInfo> vec = get_dir(dir, "");
    for (FileInfo& rec: vec) {
        std::string s = rec.fname + " " + std::to_string(rec.size) + " " + std::to_string(rec.time);
        std::cout << s.c_str() << std::endl;
    }
}

void split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

std::vector<FileInfo> readFromFileToVec(const std::string fname)
{
    std::ifstream infile(fname);
    std::string line;
    bool good;
    std::vector<FileInfo> vec;
    do {
        good = getline(infile, line).good();
        if (!good && line == "") break;
        std::vector<std::string> elems;
        split(line, ' ', elems);
        FileInfo rec;
        rec.fname = elems[0];
        rec.size = boost::lexical_cast<int64_t>(elems[1]);
        rec.time = boost::lexical_cast<int64_t>(elems[2]);
        vec.push_back(rec);
    } while (good);
    return vec;
}

void compare(const std::string srclist, const std::string destlist)
{
    std::vector<FileInfo> vec0 = readFromFileToVec(srclist);
    std::vector<FileInfo> vec1 = readFromFileToVec(destlist);
    int i0 = 0, i1 = 0;
    int64_t sumSize = 0;
    while (i0 < vec0.size() && i1 < vec1.size())
    {
        if (vec0[i0].fname < vec1[i1].fname)
        {
            printf("ADD %s %ld %ld\n", vec0[i0].fname.c_str(), vec0[i0].size, vec0[i0].time);
            sumSize += vec0[i0].size;
            i0++;
        }
        else if (vec0[i0].fname > vec1[i1].fname)
        {
            printf("DELETE %s %ld %ld\n", vec1[i1].fname.c_str(), vec1[i1].size, vec1[i1].time);
            i1++;
        }
        else
        {
            if (vec0[i0].size != vec1[i1].size) {
                printf("REPLACE %s %ld %ld\n", vec0[i0].fname.c_str(), vec0[i0].size, vec0[i0].time);
                sumSize += vec0[i0].size;
            }
            else if (vec0[i0].time != vec1[i1].time)
            {
                printf("REPLACE %s %ld %ld\n", vec0[i0].fname.c_str(), vec0[i0].size, vec0[i0].time);
                sumSize += vec0[i0].size;
            }
            i0++;
            i1++;
        }
    }
    //endings
    while (i0 < vec0.size())
    {
        printf("ADD %s %ld %ld\n", vec0[i0].fname.c_str(), vec0[i0].size, vec0[i0].time);
        sumSize += vec0[i0].size;
        i0++;
    }
    while (i1 < vec1.size())
    {
        printf("DELETE %s %ld %ld\n", vec1[i1].fname.c_str(), vec1[i1].size, vec1[i1].time);
        i1++;
    }
}

void deleteFromList(const std::string difflist, const std::string destdir)
{
    std::ifstream infile(difflist);
    std::string line;
    bool good;
    do {
        good = getline(infile, line).good();
        if (!good && line == "") break;
        std::vector<std::string> elems;
        split(line, ' ', elems);
        if (elems[0]=="DELETE" || elems[0]=="REPLACE") {
            std::string destPath = destdir + elems[1];
            if (boost::filesystem::remove(destPath))
                printf("delete %s\n", destPath.c_str());
        }
    } while (good);
}

void copyFromList(const std::string difflist, const std::string srcdir, const std::string destdir)
{
    std::ifstream infile(difflist);
    std::string line;
    bool good;
    do {
        good = getline(infile, line).good();
        if (!good && line == "") break;
        std::vector<std::string> elems;
        split(line, ' ', elems);
        if (elems[0]=="ADD" || elems[0]=="REPLACE") {
            std::string outputPath = destdir + elems[1];
            int pos = outputPath.find_last_of('/');
            std::string dir = outputPath.substr(0, pos);
            printf("copy %s to %s dir = %s\n", (srcdir + elems[1]).c_str(), (destdir + elems[1]).c_str(), dir.c_str());
            boost::filesystem::create_directories(dir);
            boost::filesystem::copy_file(srcdir + elems[1],destdir + elems[1],boost::filesystem::copy_option::fail_if_exists);
        }
    } while (good);
}

void settime(const std::string difflist, const std::string destdir)
{
    std::ifstream infile(difflist);
    std::string line;
    bool good;
    do {
        good = getline(infile, line).good();
        if (!good && line == "") break;
        std::vector<std::string> elems;
        split(line, ' ', elems);
        if (elems[0]=="ADD" || elems[0]=="REPLACE") {
            std::string destPath = destdir + elems[1];
            printf("set time to src for %s\n", destPath.c_str());
            struct stat foo;
            time_t mtime;
            struct utimbuf new_times;
            stat(destPath.c_str(), &foo);
            new_times.actime = foo.st_atime;
            new_times.modtime = boost::lexical_cast<int64_t>(elems[3]);
            utime(destPath.c_str(), &new_times);
        }
    } while (good);
}

void initsettime(const std::string srcList, const std::string destDir)
{
    std::vector<FileInfo> vec0 = readFromFileToVec(srcList);
    std::vector<FileInfo> vec1 = get_dir(destDir, "");
    int i0 = 0, i1 = 0;
    int64_t sumSize = 0;
    while (i0 < vec0.size() && i1 < vec1.size())
    {
        if (vec0[i0].fname < vec1[i1].fname)
        {
            i0++;
        }
        else if (vec0[i0].fname > vec1[i1].fname)
        {
            i1++;
        }
        else
        {
            if (vec0[i0].size == vec1[i1].size)
            {
                std::string destPath = destDir+vec1[i1].fname;
                printf("set time to src for %s\n", destPath.c_str());
                struct stat foo;
                time_t mtime;
                struct utimbuf new_times;
                stat(destPath.c_str(), &foo);
                new_times.actime = foo.st_atime;
                new_times.modtime = vec0[i0].time;    /* set mtime to original time */
                utime(destPath.c_str(), &new_times);
            }
            else throw "file sizes differ, use forgetsettime";
            i0++;
            i1++;
        }
    }
}

void forgetsettime(const std::string srcList, const std::string destDir,
                    const std::string fromTimeDiffStr,
                    const std::string toTimeDiffStr)
{
    int fromTimeDiff = boost::lexical_cast<int>(fromTimeDiffStr);
    int toTimeDiff = boost::lexical_cast<int>(toTimeDiffStr);
    std::vector<FileInfo> vec0 = readFromFileToVec(srcList);
    std::vector<FileInfo> vec1 = get_dir(destDir, "");
    int i0 = 0, i1 = 0;
    int64_t sumSize = 0;
    while (i0 < vec0.size() && i1 < vec1.size())
    {
        if (vec0[i0].fname < vec1[i1].fname)
        {
            i0++;
        }
        else if (vec0[i0].fname > vec1[i1].fname)
        {
            i1++;
        }
        else
        {
            if (vec0[i0].size == vec1[i1].size)
            {
                int diff = vec1[i1].time-vec0[i0].time;
                if (diff>=fromTimeDiff && diff<=toTimeDiff)
                {
                    std::string destPath = destDir+vec1[i1].fname;
                    printf("set time to src for %s\n", destPath.c_str());
                    struct stat foo;
                    time_t mtime;
                    struct utimbuf new_times;
                    stat(destPath.c_str(), &foo);
                    new_times.actime = foo.st_atime;
                    new_times.modtime = vec0[i0].time;    /* set mtime to original time */
                    utime(destPath.c_str(), &new_times);
                }
            }
            i0++;
            i1++;
        }
    }
}

int main(int argc, char * argv[])
{
    if (argc<2)
    {
        info();
        exit(1);
    }
    if (strcmp(argv[1], "-list") == 0) {
        list(argv[2]);
    }
    else if (strcmp(argv[1], "-compare") == 0) {
        compare(argv[2], argv[3]);
    }
    else if (strcmp(argv[1], "-delete") == 0) {
        deleteFromList(argv[2], argv[3]);
    }
    else if (strcmp(argv[1], "-copy") == 0) {
        copyFromList(argv[2], argv[3], argv[4]);
    }
    else if (strcmp(argv[1], "-settime") == 0) {
        settime(argv[2], argv[3]);
    }
    else if (strcmp(argv[1], "-initsettime") == 0) {
        initsettime(argv[2], argv[3]);
    }
    else if (strcmp(argv[1], "-forgetsettime") == 0) {
        forgetsettime(argv[2], argv[3], argv[4], argv[5]);
    }
    else {
        printf("bad command: %s\n",argv[1]);
        exit(1);
    }
    return 0;
}
